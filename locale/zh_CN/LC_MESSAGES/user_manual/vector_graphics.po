msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___vector_graphics.pot\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: 鼠标右键"

#: ../../user_manual/vector_graphics.rst:1
msgid "Overview of vector graphics in Krita."
msgstr "介绍 Krita 里面的矢量图形功能。"

#: ../../user_manual/vector_graphics.rst:11
msgid "Vector"
msgstr ""

#: ../../user_manual/vector_graphics.rst:16
msgid "Vector Graphics"
msgstr "矢量图形"

#: ../../user_manual/vector_graphics.rst:18
msgid ""
"Krita 4.0 has had a massive rewrite of the vector tools. So here's a page "
"explaining the vector tools:"
msgstr ""
"Krita 4.0 对矢量工具进行了大幅重写。我们将在本页面对全新的矢量工具进行介绍："

#: ../../user_manual/vector_graphics.rst:21
msgid "What are vector graphics?"
msgstr "矢量图形介绍"

#: ../../user_manual/vector_graphics.rst:23
msgid ""
"Krita is primarily a raster graphics editing tool, which means that most of "
"the editing changes the values of the pixels on the raster that makes up the "
"image."
msgstr ""
"Krita 总体来说是一个栅格图像编辑工具，它主要针对图像的栅格数据进行像素编辑操"
"作。"

#: ../../user_manual/vector_graphics.rst:26
msgid ".. image:: images/Pixels-brushstroke.png"
msgstr ""

#: ../../user_manual/vector_graphics.rst:27
msgid ""
"Vector graphics on the other hand use mathematics to describe a shape. "
"Because it uses a formula, vector graphics can be resized to any size."
msgstr ""
"和使用像素的栅格图像不同，矢量图形通过数学运算来描述形状。而正是因为它使用的"
"是数学算式，矢量图形可以被缩放成任意尺寸而不会损失精度。"

#: ../../user_manual/vector_graphics.rst:29
msgid ""
"On one hand, this makes vector graphics great for logos and banners. On the "
"other hand, raster graphics are much easier to edit, so vectors tend to be "
"the domain of deliberate design, using a lot of precision."
msgstr ""
"上述特性让矢量图形特别适合用于制作徽标和广告横幅。但另一方面，它编辑起来却比"
"栅格图像麻烦得多。所以矢量图形往往只会被用于那些需要严格控制尺寸的设计上面。"

#: ../../user_manual/vector_graphics.rst:32
msgid "Tools for making shapes"
msgstr "创建矢量图形的工具"

#: ../../user_manual/vector_graphics.rst:34
msgid ""
"You can start making vector graphics by first making a vector layer (press "
"the arrow button next to the :guilabel:`+` in the layer docker to get extra "
"layer types). Then, all the usual drawing tools outside of the freehand, "
"dynamic and the multibrush tool can be used to draw shapes."
msgstr ""
"要创建矢量图形，首先要新建一个矢量图层。点击图层面板左下角的 :guilabel:`+` 按"
"钮旁边的小箭头，从弹出的菜单中选择“矢量图层”。在矢量图层上面，你可以使用除了"
"手绘笔刷工具、力学笔刷工具、多重笔刷工具以外的全部常规绘画工具来绘制矢量形"
"状。"

#: ../../user_manual/vector_graphics.rst:36
msgid ""
"The path and polyline tool are the tools you used most often on a vector "
"layer, as they allow you to make the most dynamic of shapes."
msgstr ""
"在矢量图层上你用得最多的工具应该是路径工具和折线工具，它们可以让你绘制出变化"
"多端的形状。"

#: ../../user_manual/vector_graphics.rst:38
msgid ""
"On the other hand, the :guilabel:`Ellipse` and :guilabel:`Rectangle` tools "
"allow you to draw special shapes, which then can be edited to make special "
"pie shapes, or for easy rounded rectangles."
msgstr ""
"另一方面， :guilabel:`椭圆工具` 和 :guilabel:`矩形工具` 可以画出某些特定的形"
"状。你可以用椭圆工具来绘制饼状图，而矩形工具还可以很方便地生成圆角。"

#: ../../user_manual/vector_graphics.rst:40
msgid ""
"The calligraphy and text tool also make special vectors. The calligraphy "
"tool is for producing strokes that are similar to brush strokes, while the "
"text tool makes a text object that can be edited afterwards."
msgstr ""
"你还可以通过西文书法工具和文字工具来创建特别的矢量图形。西文书法工具可以画出"
"类似于手绘笔刷的矢量形状，而文字工具则用于制作可随时编辑的文字对象。"

#: ../../user_manual/vector_graphics.rst:42
msgid ""
"All of these will use the current brush size to determine stroke thickness, "
"as well as the current foreground and background color."
msgstr ""
"所有上述工具都会使用当前笔刷大小、前景色和背景色等来决定生成图形的样式。"

#: ../../user_manual/vector_graphics.rst:44
msgid ""
"There is one last way to make vectors: the :guilabel:`Vector Image` tool.  "
"It allows you to add shapes that have been defined in an SVG file as "
"symbols. Unlike the other tools, these have their own fill and stroke."
msgstr ""
"最后，我们还可以使用 :guilabel:`矢量图库` 工具面板来添加预制的 SVG 文件。和其"
"他工具不同的是通过矢量图库插入的文件就像是符号一样，有它们自己的一套填充色和"
"描边色。"

#: ../../user_manual/vector_graphics.rst:47
msgid "Arranging Shapes"
msgstr "排列形状"

#: ../../user_manual/vector_graphics.rst:49
msgid ""
"A vector layer has its own hierarchy of shapes, much like how the whole "
"image has a hierarchy of layers. So shapes can be in front of one another. "
"This can be modified with the arrange docker, or with the :guilabel:`Select "
"Shapes` tool."
msgstr ""
"每个矢量图层所包含的图形都有一套层级关系，这就像是图层的层级关系一样。矢量形"
"状可以互相遮挡，我们可以使用“矢量图形排列”工具面板或者 :guilabel:`形状选取工"
"具` 来对矢量图形进行排序。"

#: ../../user_manual/vector_graphics.rst:51
msgid ""
"The arrange docker also allows you to group and ungroup shapes. It also "
"allows you to precisely align shapes, for example, have them aligned to the "
"center, or have an even spacing between all the shapes."
msgstr ""
"你还可以通过矢量图形排列面板组合或者拆散形状，对各个形状进行精确对齐，例如把"
"它们沿中心对齐，或者以均匀间距分布等。"

#: ../../user_manual/vector_graphics.rst:54
msgid "Editing shapes"
msgstr "编辑形状"

#: ../../user_manual/vector_graphics.rst:56
msgid ""
"Editing of vector shapes is done with the :guilabel:`Select Shapes` tool and "
"the :guilabel:`Edit Shapes` tool."
msgstr ""
"要编辑矢量图形，可使用 :guilabel:`形状选取工具` 和 :guilabel:`形状编辑工具"
"` 。"

#: ../../user_manual/vector_graphics.rst:58
msgid ""
"The :guilabel:`Select Shapes` tool can be used to select vector shapes, to "
"group them (via |mouseright|), ungroup them, to use booleans to combine or "
"subtract shapes from one another (via |mouseright|), to move them up and "
"down, or to do quick transforms."
msgstr ""
":guilabel:`形状选取工具` 可以用来选择形状和进行简单变形，通过右键菜单 |"
"mouseright| ，还可以组合/拆散形状、调整图形的先后顺序、使用布尔逻辑来对形状进"
"行结合、减去等操作。"

#: ../../user_manual/vector_graphics.rst:61
msgid "Fill"
msgstr "填充"

#: ../../user_manual/vector_graphics.rst:63
msgid ""
"You can change the fill of a shape by selecting it and changing the active "
"foreground color."
msgstr "要更改一个矢量形状的填充色，可以先选中它，然后更改当前的前景色。"

#: ../../user_manual/vector_graphics.rst:65
msgid ""
"You can also change it by going into the tool options of the :guilabel:"
"`Select Shapes` tool and going to the :guilabel:`Fill` tab."
msgstr ""
"你还可以在 :guilabel:`形状选取工具` 的工具选项切换到 :guilabel:`填充` 页面更"
"改填充色。"

#: ../../user_manual/vector_graphics.rst:67
msgid ""
"Vector shapes can be filled with a solid color, a gradient or a pattern."
msgstr "矢量图形可以用实色、渐变和图案进行填充。"

#: ../../user_manual/vector_graphics.rst:70
msgid "Stroke"
msgstr "描边"

#: ../../user_manual/vector_graphics.rst:72
msgid "Strokes can be filled with the same things as fills."
msgstr "矢量图像的描边色也具有和填充一样的各种选项。"

#: ../../user_manual/vector_graphics.rst:74
msgid ""
"However, they can also be further changed. For example, you can add dashes "
"and markers to the line."
msgstr ""
"不过描边还有别的选项来提供更加丰富的变化，例如把线条换成虚线或者在线条上面加"
"入箭头。"

#: ../../user_manual/vector_graphics.rst:77
msgid "Coordinates"
msgstr "坐标"

#: ../../user_manual/vector_graphics.rst:79
msgid ""
"Shapes can be moved with the :guilabel:`Select Shapes` tool, and in the tool "
"options you can specify the exact coordinates."
msgstr ""
"要移动矢量形状，可以使用 :guilabel:`形状选取工具` ，你可以在它的工具选项中指"
"定确切的坐标数值。"

#: ../../user_manual/vector_graphics.rst:82
msgid "Editing nodes and special parameters"
msgstr "编辑节点和特有参数"

#: ../../user_manual/vector_graphics.rst:84
msgid ""
"If you have a shape selected, you can double click it to get to the "
"appropriate tool to edit it. Usually this is the :guilabel:`Edit Shape` "
"tool, but for text this is the :guilabel:`Text` tool."
msgstr ""
"在选中了一个矢量形状时，对它再次双击可以切换到用于编辑该形状的对应工具。一般"
"情况下这会切换到 :guilabel:`形状编辑工具` ，而双击文字形状时则会切换到 :"
"guilabel:`文字` 工具。"

#: ../../user_manual/vector_graphics.rst:86
msgid ""
"In the :guilabel:`Edit Shape` tool, you can move around nodes on the canvas "
"for regular paths. For special paths, like the ellipse and the rectangle, "
"you can move nodes and edit the specific parameters in the :guilabel:`Tool "
"Options` docker."
msgstr ""
"在使用 :guilabel:`形状编辑工具` 时，你可以在画布上移动一般路径的节点。而特殊"
"的路径，如椭圆和矩形路径等，除了移动节点之外还可以在 :guilabel:`工具选项` 面"
"板里面编辑它们的特有参数。"

#: ../../user_manual/vector_graphics.rst:89
msgid "Working together with other programs"
msgstr "与其他程序配合工作"

#: ../../user_manual/vector_graphics.rst:91
msgid ""
"One of the big things Krita 4.0 brought was moving from ODG to SVG. What "
"this means is that Krita saves as SVG inside KRA files, and that means we "
"can open SVGs just fine. This is important as SVG is the most popular vector "
"format."
msgstr ""
"Krita 从 4.0 版开始从 ODG 规范转向 SVG 规范。Krita 会把矢量图形按 SVG 格式保"
"存在 KRA 文件中，也可以正常打开 SVG 文件。这项变化之所以值得一提，是因为 SVG "
"是目前适用范围最广的矢量图像格式。"

#: ../../user_manual/vector_graphics.rst:94
msgid "Inkscape"
msgstr "Inkscape"

#: ../../user_manual/vector_graphics.rst:96
msgid ""
"You can copy and paste vectors from Krita to Inkscape, or from Inkscape to "
"Krita. Only the SVG 1.1 features are supported, so don't be surprised if a "
"mesh gradient doesn't cross over very well."
msgstr ""
"你可以把矢量图形从 Krita 复制粘贴到 Inkscape，也可以反过来从 Inkscape 复制粘"
"贴到 Krita。不过 Krita 仅支持 SVG 1.1 版的特性，如果使用了网格渐变，跨程序转"
"移后的效果无法保证一致。"
