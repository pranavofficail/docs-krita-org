msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___animation.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: 鼠标左键"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: 鼠标右键"

#: ../../user_manual/animation.rst:1
msgid "Detailed guide on the animation workflow in Krita."
msgstr "详细介绍 Krita 的动画制作流程。"

#: ../../user_manual/animation.rst:13
msgid "Animation"
msgstr ""

#: ../../user_manual/animation.rst:18
msgid "Animation with Krita"
msgstr "动画制作"

#: ../../user_manual/animation.rst:20
msgid ""
"Thanks to the 2015 Kickstarter, :program:`Krita 3.0` now has animation. In "
"specific, :program:`Krita` has frame-by-frame raster animation. There's "
"still a lot of elements missing from it, like tweening, but the basic "
"workflow is there."
msgstr ""
"作为 2015 年度 Kickstarter 筹款活动的一项重要目标， :program:`Krita 3.0` 实现"
"了动画制作功能，可用于制作逐帧栅格动画。尽管依然缺少了自动补间等功能，但最基"
"本的动画制作流程已经就位了。"

#: ../../user_manual/animation.rst:25
msgid ""
"To access the animation features, the easiest way is to change your "
"workspace to Animation. This will make the animation dockers and workflow "
"appear."
msgstr ""
"要使用动画功能，最直接的办法就是把工作空间更改为“Animation (动画)”，它会按照"
"动画制作流程的需要显示相关工具面板并重新安排它们的位置。"

#: ../../user_manual/animation.rst:30
msgid "Animation curves"
msgstr "动画曲线"

#: ../../user_manual/animation.rst:32
msgid ""
"To create an animation curve (currently only for opacity) expand the :"
"guilabel:`New Frame` button in the :guilabel:`Animation` dock and click :"
"guilabel:`Add Opacity Keyframe`. You can now edit the keyframed value for "
"opacity directly in the “Layers” dock, adding more keyframes will by default "
"fade from the last to the next upcoming keyframe in the timeline over the "
"frames between them. See :ref:`animation curves <animation_curves_docker>` "
"for details."
msgstr ""
"要创建动画曲线 (目前仅支持不透明度)，可在:guilabel:`动画` 面板点击 :guilabel:"
"`创建空白帧` 按钮右侧的下箭头，在弹出的菜单中选择 :guilabel:`添加不透明度关键"
"帧` 。然后你便可以在“图层”面板中直接编辑该关键帧的不透明度。位于两个不透明度"
"关键帧之间的各关键帧透明度将自动过渡。详情请见 :ref:`动画曲线 "
"<animation_curves_docker>` 章节。"

#: ../../user_manual/animation.rst:40
msgid "Workflow"
msgstr "制作流程"

#: ../../user_manual/animation.rst:42
msgid ""
"In traditional animation workflow, what you do is that you make *key "
"frames*, which contain the important poses, and then draw frames in between "
"(\\ *tweening* in highly sophisticated animator's jargon)."
msgstr ""
"在传统动画的制作流程中，动画师首先要绘制 **关键帧** (也叫“原画”)，它们包含了"
"重要的镜头和动作。接下来他们会在这些关键帧之间绘制过渡的动画帧 (也叫“中间"
"画”)。"

#: ../../user_manual/animation.rst:46
msgid "For this workflow, there are three important dockers:"
msgstr "Krita 的动画制作流程要用到三个重要的工具面板："

#: ../../user_manual/animation.rst:48
msgid ""
"The :ref:`timeline_docker`. View and control all of the frames in your "
"animation. The timeline docker also contains functions to manage your "
"layers. The layer that are created in the timeline docker also appear on the "
"normal Layer docker."
msgstr ""
":ref:`timeline_docker` 面板。用于按时间查看和控制当前动画的全部动画帧。该面板"
"还具备图层管理功能。在时间线面板中创建的图层也会在常规图层面板上显示。"

#: ../../user_manual/animation.rst:52
msgid ""
"The :ref:`animation_docker`. This docker contains the play buttons as the "
"ability to change the frame-rate, playback speed and useful little options "
"like :guilabel:`auto-key framing`."
msgstr ""
":ref:`animation_docker` 面板。提供播放和切换动画帧的按钮，可以更改帧率、回放"
"速度和 :guilabel:`自动帧` 模式等选项。"

#: ../../user_manual/animation.rst:55
msgid ""
"The :ref:`onion_skin_docker`. This docker controls the look of the onion "
"skin, which in turn is useful for seeing the previous frame."
msgstr ""
":ref:`onion_skin_docker` 面板。控制洋葱皮视图的显示方式，用于辅助观察当前帧的"
"过去帧和未来帧。"

#: ../../user_manual/animation.rst:60
msgid "Introduction to animation: How to make a walkcycle"
msgstr "动画入门：如何绘制步态循环"

#: ../../user_manual/animation.rst:62
msgid ""
"The best way to get to understand all these different parts is to actually "
"use them. Walk cycles are considered the most basic form of a full "
"animation, because of all the different parts involved with them. Therefore, "
"going over how one makes a walkcycle should serve as a good introduction."
msgstr ""
"要想熟悉 Krita 的动画功能，最好的办法就是使用它们来进行动画制作的实践。步态循"
"环是全动画的最基础形态，它会涉及到动画制作的每一个方面，因此我们可以通过制作"
"一组步态循环来入门 Krita 的动画功能。"

#: ../../user_manual/animation.rst:69
msgid "Setup"
msgstr "初始设置"

#: ../../user_manual/animation.rst:71
msgid "First, we make a new file:"
msgstr "首先我们要创建一个新文档："

#: ../../user_manual/animation.rst:74
msgid ".. image:: images/animation/Introduction_to_animation_01.png"
msgstr ""

#: ../../user_manual/animation.rst:75
msgid ""
"On the first tab, we type in a nice ratio like 1280x1024, set the dpi to 72 "
"(we're making this for screens after all) and title the document 'walkcycle'."
msgstr ""
"打开新建文档对话框，在自定义文档的尺寸页面上，把文档尺寸设为 1280 x 1024，分"
"辨率设为 72 ppi (因为它将用于屏幕显示)，把文档名字命名为“walkcycle” (步态循"
"环)。"

#: ../../user_manual/animation.rst:79
msgid ""
"In the second tab, we choose a nice background color, and set the background "
"to canvas-color. This means that Krita will automatically fill in any "
"transparent bits with the background color. You can change this in :"
"menuselection:`Image --> Image Properties`. This seems to be most useful to "
"people doing animation, as the layer you do animation on MUST be semi-"
"transparent to get onion skinning working."
msgstr ""
"接下来我们要切换到内容页面。在这里指定一个看起来舒服的背景色，然后选中“作为画"
"布颜色”。这样 Krita 会在背景的透明区域填充这种背景色。你还可以在菜单栏的 :"
"menuselection:`图像 --> 属性` 处对此进行更改。这一设置对于动画制作尤为重要，"
"因为洋葱皮视图必须在含有透明度的图层上才能正常显示。"

#: ../../user_manual/animation.rst:82
msgid ""
"Krita has a bunch of functionality for meta-data, starting at the :guilabel:"
"`Create Document` screen. The title will be automatically used as a "
"suggestion for saving and the description can be used by databases, or for "
"you to leave comments behind. Not many people use it individually, but it "
"can be useful for working in larger groups."
msgstr ""
"Krita 有一系列与元数据相关的功能。你可以在 :guilabel:`创建新文档` 对话框里面"
"编辑部分元数据。文档标题可以在保存图像时自动用作候选文件名，而描述则可以被数"
"据库抓取，也可以用来留言。尽管单人匹马的作者应该用不到这些功能，但在大团队协"
"作时它们将会派上用场。"

#: ../../user_manual/animation.rst:84
msgid "Then hit :guilabel:`Create`!"
msgstr "点击 :guilabel:`创建` 按钮，完成新文档的创建吧！"

#: ../../user_manual/animation.rst:86
msgid ""
"Then, to get all the necessary tools for animation, select the workspace "
"switcher:"
msgstr ""
"现在，我们要让 Krita 显示动画制作流程所需的工具面板。点击窗口右上角的“工作空"
"间选单”按钮："

#: ../../user_manual/animation.rst:91
msgid ".. image:: images/animation/Introduction_to_animation_02.png"
msgstr ""

#: ../../user_manual/animation.rst:91
msgid "The red arrow points at the workspace switcher."
msgstr "红色箭头所指的就是“工作空间选单”按钮。"

#: ../../user_manual/animation.rst:93
msgid "And select the animation workspace."
msgstr "从弹出的工作空间列表中选择 “Animation” (动画)。"

#: ../../user_manual/animation.rst:95
msgid "Which should result in this:"
msgstr "工作空间将会变为下图的样式："

#: ../../user_manual/animation.rst:98
msgid ".. image:: images/animation/Introduction_to_animation_03.png"
msgstr ""

#: ../../user_manual/animation.rst:99
msgid ""
"The animation workspace adds the timeline, animation and onion skin dockers "
"at the bottom."
msgstr ""
"动画工作空间会在窗口的底部显示时间线面板和动画面板。要显示洋葱皮面板，点击动"
"画面板的“洋葱”按钮。"

#: ../../user_manual/animation.rst:103
msgid "Animating"
msgstr "绘制动画"

#: ../../user_manual/animation.rst:105
msgid ""
"We have two transparent layers set up. Let's name the bottom one "
"'environment' and the top 'walkcycle' by double clicking their names in the "
"layer docker."
msgstr ""
"我们刚才新建的文档里面有两张透明图层。双击图层名称，把下面的图层重命名"
"为“environment” (环境)，把上面的图层重命名为“walkcycle” (步态循环)。"

#: ../../user_manual/animation.rst:110
msgid ".. image:: images/animation/Introduction_to_animation_04.png"
msgstr ""

#: ../../user_manual/animation.rst:111
msgid ""
"Use the straight line tool to draw a single horizontal line. This is the "
"ground."
msgstr "用直线工具画一条水平线，把它作为地面。"

#: ../../user_manual/animation.rst:115
msgid ".. image:: images/animation/Introduction_to_animation_05.png"
msgstr ""

#: ../../user_manual/animation.rst:116
msgid ""
"Then, select the 'walkcycle' layer and draw a head and torso (you can use "
"any brush for this)."
msgstr ""
"然后选中 walkcycle (步态循环) 图层，随便选择一种笔刷，把人物的头部和躯干画出"
"来。"

#: ../../user_manual/animation.rst:118
msgid ""
"Now, selecting a new frame will not make a new frame automatically. Krita "
"doesn't actually see the 'walkcycle' layer as an animated layer at all!"
msgstr ""
"接下来让我们把注意力转向时间轴面板。我们可以看到时间轴里面空无一物。这是因为"
"我们还没有把当前图层添加成一个动画帧，所以 Krita 不把它视作动画的一部分。"

#: ../../user_manual/animation.rst:123
msgid ".. image:: images/animation/Introduction_to_animation_06.png"
msgstr ""

#: ../../user_manual/animation.rst:124
msgid ""
"We can make it animatable by adding a frame to the timeline. |mouseright| a "
"frame in the timeline to get a context menu. Choose :guilabel:`New Frame`."
msgstr ""
"想要把图层变为动画的一部分，我们必须先在时间线上添加一个动画帧。在时间线的空"
"槽上右键单击 |mouseright| ，在右键菜单中选中 :guilabel:`创建空白帧`。"

#: ../../user_manual/animation.rst:128
msgid ".. image:: images/animation/Introduction_to_animation_07.png"
msgstr ""

#: ../../user_manual/animation.rst:129
msgid ""
"You can see it has become an animated layer because of the onion skin icon "
"showing up in the timeline docker."
msgstr ""
"你会发现这个图层的时间轴左边多了一个灯泡状的图标，它是洋葱皮视图的开关。这意"
"味着这个图层已经成为了动画图层。"

#: ../../user_manual/animation.rst:133
msgid ".. image:: images/animation/Introduction_to_animation_08.png"
msgstr ""

#: ../../user_manual/animation.rst:134
msgid ""
"Use the :guilabel:`Copy Frame` button to copy the first frame onto the "
"second. Then, use the with the :kbd:`Shift + ↑` shortcut to move the frame "
"contents up."
msgstr ""

#: ../../user_manual/animation.rst:137
msgid "We can see the difference by turning on the onionskinning:"
msgstr "现在点击时间线左边的灯泡图标，启用洋葱皮显示："

#: ../../user_manual/animation.rst:140
msgid ".. image:: images/animation/Introduction_to_animation_09.png"
msgstr ""

#: ../../user_manual/animation.rst:141
msgid "Now, you should see the previous frame as red."
msgstr "你会发现前一帧的内容被显示为红色。"

#: ../../user_manual/animation.rst:144
msgid ""
"Krita sees white as a color, not as transparent, so make sure the animation "
"layer you are working on is transparent in the bits where there's no "
"drawing. You can fix the situation by use the :ref:`filter_color_to_alpha` "
"filter, but prevention is best."
msgstr ""
"Krita 把白色也视作一种颜色，而不是把它当成透明。为了正常显示洋葱皮视图，动画"
"图层在没有内容的区域上最好保持透明。如果你不小心在整个动画图层上面填充了底"
"色，可以使用 :ref:`filter_color_to_alpha` 滤镜来补救。但比起亡羊补牢，防范于"
"未然更加重要。"

#: ../../user_manual/animation.rst:147
msgid ".. image:: images/animation/Introduction_to_animation_10.png"
msgstr ""

#: ../../user_manual/animation.rst:148
msgid ""
"Future frames are drawn in green, and both colors can be configured in the "
"onion skin docker."
msgstr ""
"未来帧的内容则会被显示为绿色。过去帧和未来帧的显示颜色可以在洋葱皮面板中进行"
"更改。要显示洋葱皮面板，点击动画面板的“洋葱”按钮。"

#: ../../user_manual/animation.rst:152
msgid ".. image:: images/animation/Introduction_to_animation_11.png"
msgstr ""

#: ../../user_manual/animation.rst:153
msgid ""
"Now, we're gonna draw the two extremes of the walkcycle. These are the pose "
"where both legs are as far apart as possible, and the pose where one leg is "
"full stretched and the other pulled in, ready to take the next step."
msgstr ""
"接下来我们要画出步态循环的两个极端动作。第一个动作是两脚迈得最开的瞬间；第二"
"个动作是一条腿完全伸直，另一条腿已经收起准备向前迈出的瞬间。"

#: ../../user_manual/animation.rst:158
msgid ""
"Now, let's copy these two... We could do that with the :kbd:`Ctrl + drag` "
"shortcut, but here comes a tricky bit:"
msgstr ""

#: ../../user_manual/animation.rst:162
msgid ".. image:: images/animation/Introduction_to_animation_12.png"
msgstr ""

#: ../../user_manual/animation.rst:163
msgid ""
":kbd:`Ctrl +` |mouseleft| also selects and deselects frames, so to copy..."
msgstr "现在让我们再试一次，把动画帧 0、1 复制到时间槽 2、3："

#: ../../user_manual/animation.rst:165
msgid ":kbd:`Ctrl +` |mouseleft| to select all the frames you want to select."
msgstr ""
"首先，用 :kbd:`Ctrl + 鼠标左键` |mouseleft| 来选中需要复制的帧。被选中的帧会"
"显示为橙色，如上图下半部分所示。别忘了：在拖动选择时，最后一个帧不会被选中。"

#: ../../user_manual/animation.rst:166
msgid ""
":kbd:`Ctrl + drag`. You need to make sure the first frame is 'orange', "
"otherwise it won't be copied along."
msgstr "接着，用 :kbd:`Ctrl + 拖放` 把动画帧 0、1 复制到时间槽 2、3。"

#: ../../user_manual/animation.rst:169
msgid "Now then..."
msgstr "接下来我们要播放这个循环："

#: ../../user_manual/animation.rst:174
msgid ".. image:: images/animation/Introduction_to_animation_13.png"
msgstr ""

#: ../../user_manual/animation.rst:174
msgid "squashed the timeline docker a bit to save space"
msgstr ""
"如果左侧工具面板空间不够，你可以把动画面板和洋葱皮面板拖放到底侧空间，和时间"
"线面板并排。"

#: ../../user_manual/animation.rst:176
msgid "Copy frame 0 to frame 2."
msgstr "确认动画帧 0 已被复制到 帧 2.。"

#: ../../user_manual/animation.rst:177
msgid "Copy frame 1 to frame 3."
msgstr "确认动画帧 1 已被复制到 帧 3.。"

#: ../../user_manual/animation.rst:178
msgid "In the animation docker, set the frame-rate to 4."
msgstr "在动画面板中，把帧率设为 4。"

#: ../../user_manual/animation.rst:179
msgid "Select all frames in the timeline docker by dragging-selecting them."
msgstr "用 Ctrl + 拖动在时间线里面选中所有 4 个帧。"

#: ../../user_manual/animation.rst:180
msgid "Press play in the animation docker."
msgstr "点击动画面板中的“播放”按钮。"

#: ../../user_manual/animation.rst:181
msgid "Enjoy your first animation!"
msgstr "你的第一条动画就做好了！"

#: ../../user_manual/animation.rst:184
msgid "Expanding upon your rough walkcycle"
msgstr "添加并绘制中间画"

#: ../../user_manual/animation.rst:187
msgid ".. image:: images/animation/Introduction_to_animation_14.png"
msgstr ""

#: ../../user_manual/animation.rst:188
msgid ""
"You can quickly make some space by the :kbd:`Alt + drag` shortcut on any "
"frame. This'll move that frame and all others after it in one go."
msgstr ""

#: ../../user_manual/animation.rst:191
msgid "Then draw inbetweens on each frame that you add."
msgstr "然后在空出来的时间槽中插入并绘制中间帧。"

#: ../../user_manual/animation.rst:194
msgid ".. image:: images/animation/Introduction_to_animation_16.png"
msgstr ""

#: ../../user_manual/animation.rst:195
msgid ""
"You'll find that the more frames you add, the more difficult it becomes to "
"keep track of the onion skins."
msgstr "不过你很快会发现，插入的帧越多，用洋葱皮追踪前后帧就越困难。"

#: ../../user_manual/animation.rst:197
msgid ""
"You can modify the onion skin by using the onion skin docker, where you can "
"change how many frames are visible at once, by toggling them on the top row. "
"The bottom row is for controlling transparency, while below there you can "
"modify the colors and extremity of the coloring."
msgstr ""
"你可以用洋葱皮面板对洋葱皮的显示方式进行修改。点击顶栏的数字可以控制该帧数距"
"离的洋葱皮显示开关，数字越大，意味着它与当前帧离得越远。数字栏下方的柱状图可"
"以控制透明度的变化。面板下方的色块和滑动条可以调整颜色和着色强度。"

#: ../../user_manual/animation.rst:203
msgid ".. image:: images/animation/Introduction_to_animation_15.png"
msgstr ""

#: ../../user_manual/animation.rst:205
msgid "Animating with multiple layers"
msgstr "制作多图层动画"

#: ../../user_manual/animation.rst:207
msgid ""
"Okay, our walkcycle is missing some hands, let's add them on a separate "
"layer. So we make a new layer, and name it hands and..."
msgstr ""
"这个步态循环还缺了一双手，我们打算在别的图层上绘制它们。我们可以在常规图层面"
"板上新建图层，也可以在时间线面板的左上角按“+”按钮，新建图层，把它命名"
"为“hands” (手)。可现在问题来了……"

#: ../../user_manual/animation.rst:211
msgid ".. image:: images/animation/Introduction_to_animation_17.png"
msgstr ""

#: ../../user_manual/animation.rst:212
msgid ""
"Our walkcycle is gone from the timeline docker! This is a feature actually. "
"A full animation can have so many little parts that an animator might want "
"to remove the layers they're not working on from the timeline docker. So you "
"manually have to add them."
msgstr ""
"刚才画的步态循环从时间线面板上消失了！不必担心，这其实是时间线和图层的协同显"
"示功能在起作用。这是因为在全动画制作流程里，画面的组成部分可能会有很多，动画"
"师会把一些当前无需处理的部分从时间线上隐藏起来。你可以手动把任意图层添加到时"
"间线。"

#: ../../user_manual/animation.rst:218
msgid ".. image:: images/animation/Introduction_to_animation_18.png"
msgstr ""

#: ../../user_manual/animation.rst:219
msgid ""
"You can show any given layer in the timeline by doing |mouseright| on the "
"layer in the layer docker, and toggling :guilabel:`Show in Timeline`."
msgstr ""
"要把一个图层显示在时间轴，在图层列表中右键单击 |mouseright| ，勾选 :guilabel:"
"`显示在时间轴` 。"

#: ../../user_manual/animation.rst:223
msgid ".. image:: images/animation/Introduction_to_animation_19.png"
msgstr ""

#: ../../user_manual/animation.rst:225
msgid "Exporting"
msgstr "导出"

#: ../../user_manual/animation.rst:227
msgid "When you are done, select :menuselection:`File --> Render Animation`."
msgstr ""
"动画制作完成后，我们还要把它进行导出。在菜单栏点击 :menuselection:`文件 --> "
"渲染动画` 。"

#: ../../user_manual/animation.rst:230
msgid ".. image:: images/animation/Introduction_to_animation_20.png"
msgstr ""

#: ../../user_manual/animation.rst:231
msgid ""
"It's recommended to save out your file as a png, and preferably in its own "
"folder. Krita can currently only export png sequences."
msgstr ""
"在渲染动画对话框，选中“图像序列”。Krita 目前只能导出 PNG 图像序列，所以要在图"
"像序列选项中把文件格式设为“PNG 图像”。为避免混乱，最好为输出的图像序列指定一"
"个单独的文件夹。"

#: ../../user_manual/animation.rst:235
msgid ".. image:: images/animation/Introduction_to_animation_21.png"
msgstr ""

#: ../../user_manual/animation.rst:236
msgid ""
"When pressing done, you can see the status of the export in the status bar "
"below."
msgstr ""
"点击“确定”按钮后 Krita 便会开始渲染动画。渲染过程可长可短，你可以在窗口底部的"
"状态栏查看导出进度。"

#: ../../user_manual/animation.rst:240
msgid ".. image:: images/animation/Introduction_to_animation_22.png"
msgstr ""

#: ../../user_manual/animation.rst:241
msgid ""
"The images should be saved out as filenameXXX.png, giving their frame number."
msgstr "图像序列将被保存成“文件名XXXX.png”的格式，XXXX后缀为该张图像的帧号。"

#: ../../user_manual/animation.rst:244
msgid ""
"Then use something like Gimp (Linux, OSX, Windows), ImageMagick (Linux, OSX, "
"Windows), or any other gif creator to make a gif out of your image sequence:"
msgstr ""
"接下来你便可以使用 GIMP、ImageMagick 等工具来把该图像序列合成为一个 GIF 动画"
"了："

#: ../../user_manual/animation.rst:249
msgid ".. image:: images/animation/Introduction_to_animation_walkcycle_02.gif"
msgstr ""

#: ../../user_manual/animation.rst:250
msgid ""
"For example, you can use `VirtualDub <http://www.virtualdub.org/>`_\\ "
"(Windows) and open all the frames and then go to :menuselection:`File --> "
"Export --> GIF`."
msgstr ""

#: ../../user_manual/animation.rst:254
msgid "Enjoy your walkcycle!"
msgstr "现在你就可以慢慢欣赏你的步态循环大作了！"

#: ../../user_manual/animation.rst:258
msgid ""
"Krita 3.1 has a render animation feature. If you're using the 3.1 beta, "
"check out the :ref:`render_animation` page for more information!"
msgstr ""
"Krita 从 3.1 版开始支持直接渲染为动画。详情请参考 :ref:`render_animation` 页"
"面。"

#: ../../user_manual/animation.rst:261
msgid "Importing animation frames"
msgstr "导入动画帧"

#: ../../user_manual/animation.rst:263
msgid "You can import animation frames in Krita 3.0."
msgstr "Krita 从 3.0 版开始支持导入动画帧。"

#: ../../user_manual/animation.rst:265
msgid ""
"First let us take a sprite sheet from Open Game Art. (This is the Libre "
"Pixel Cup male walkcycle)"
msgstr ""
"首先，让我们从 Open Game Art 网站找一张像素拼合图。(以 Libre Pixel Cup male "
"walkcycle 为例) "

#: ../../user_manual/animation.rst:268
msgid ""
"And we'll use :menuselection:`Image --> Split Image` to split up the sprite "
"sheet."
msgstr ""
"点击菜单栏的 :menuselection:`图像 --> 分割图像` ，把像素拼合图进行分割。"

#: ../../user_manual/animation.rst:271
msgid ".. image:: images/animation/Animation_split_spritesheet.png"
msgstr ""

#: ../../user_manual/animation.rst:272
msgid ""
"The slices are even, so for a sprite sheet of 9 sprites, use 8 vertical "
"slices and 0 horizontal slices. Give it a proper name and save it as png."
msgstr ""
"这个拼合图内含 9 张图，所以我们要把它分成 9 块。将垂直分割线设为 8 条，水平分"
"割线设为 0 条，随便输入一个文件名前缀，文件类型为 PNG，点击应用。图像将被分割"
"为均等大小。"

#: ../../user_manual/animation.rst:274
msgid ""
"Then, make a new canvas, and select :menuselection:`File --> Import "
"Animation Frames`. This will give you a little window. Select :guilabel:`Add "
"images`. This should get you a file browser where you can select your images."
msgstr ""
"新建一个文档，点击菜单栏的 :menuselection:`文件 --> 导入动画帧` 。在弹出的对"
"话框中点击 :guilabel:`添加图像` 按钮，从导入对话框中找到刚才分割的拼合图。"

#: ../../user_manual/animation.rst:277
msgid ".. image:: images/animation/Animation_import_sprites.png"
msgstr ""

#: ../../user_manual/animation.rst:278
msgid "You can select multiple images at once."
msgstr "你可以同时选中多个图像进行导入。"

#: ../../user_manual/animation.rst:281
msgid ".. image:: images/animation/Animation_set_everything.png"
msgstr ""

#: ../../user_manual/animation.rst:282
msgid ""
"The frames are currently automatically ordered. You can set the ordering "
"with the top-left two drop-down boxes."
msgstr ""
"即将导入的动画帧会被自动排序。你也可以在对话框右上角的“排序”选单对排序方式进"
"行调整。"

#: ../../user_manual/animation.rst:285
msgid "Start"
msgstr "首帧"

#: ../../user_manual/animation.rst:286
msgid "Indicates at which point the animation should be imported."
msgstr "此参数指定从第几帧开始导入该动画帧序列。"

#: ../../user_manual/animation.rst:288
msgid ""
"Indicates the difference between the imported animation and the document "
"frame rate. This animation is 8 frames big, and the fps of the document is "
"24 frames, so there should be a step of 3 to keep it even. As you can see, "
"the window gives feedback on how much fps the imported animation would be "
"with the currently given step."
msgstr ""
"此参数指定导入的每张图像在动画中停留的帧数。下方的“源帧率”会自动按照此数值和"
"当前文档帧率来计算出被导入动画序列的理想原始帧率。在本例中，动画文档的帧率为 "
"24，因此当帧长为 3 时，可推算出导入图像序列的理想源帧率为 8，这和我们将要导入"
"的序列是一致的。"

#: ../../user_manual/animation.rst:292
msgid "Step"
msgstr "帧长"

#: ../../user_manual/animation.rst:294
msgid ""
"Press :guilabel:`OK`, and your animation should be imported as a new layer."
msgstr "点击 :guilabel:`确定` 之后，这个图像序列会被导入到一个新的动画图层。"

#: ../../user_manual/animation.rst:297
msgid ".. image:: images/animation/Animation_import_done.png"
msgstr ""

#: ../../user_manual/animation.rst:299
msgid "Reference"
msgstr "参考资料"

#: ../../user_manual/animation.rst:301
msgid "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"
msgstr "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"

#: ../../user_manual/animation.rst:302
msgid ""
"`The source for the libre pixel cup male walkmediawiki cycle <https://"
"opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-"
"tiles>`_"
msgstr ""
"`文中例子所用的 Libre pixel cup male walkcycle <https://opengameart.org/"
"content/liberated-pixel-cup-lpc-base-assets-sprites-map-tiles>`_"
