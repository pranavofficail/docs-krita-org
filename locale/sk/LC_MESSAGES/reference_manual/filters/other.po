# translation of docs_krita_org_reference_manual___filters___other.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___other\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:31+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/filters/other.rst:1
msgid "Overview of the other filters."
msgstr ""

#: ../../reference_manual/filters/other.rst:15
msgid "Other"
msgstr "Iné"

#: ../../reference_manual/filters/other.rst:17
msgid "Filters signified by them not fitting anywhere else."
msgstr ""

#: ../../reference_manual/filters/other.rst:20
msgid "Wave"
msgstr "Vlna"

#: ../../reference_manual/filters/other.rst:22
msgid "Adds a cute little wave-distortion effect to the input image."
msgstr ""

#: ../../reference_manual/filters/other.rst:24
#: ../../reference_manual/filters/other.rst:27
msgid "Random Noise"
msgstr "Náhodný šum"

#: ../../reference_manual/filters/other.rst:29
msgid "Gives Random Noise to input image."
msgstr ""

#: ../../reference_manual/filters/other.rst:32
msgid "Random Pick"
msgstr "Náhodný výber"

#: ../../reference_manual/filters/other.rst:34
msgid "Adds a little pixely-fringe to the input image."
msgstr ""
