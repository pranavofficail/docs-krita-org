# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-07-02 12:39+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_1.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_1.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_2.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_2.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_3.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_3.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/brushes/Krita-normals-tutoria_4.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:1
msgid "The Tangent Normal Brush Engine manual page."
msgstr "De handleidingpagina Raaklijnnormaalpenseel-engine."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:16
msgid "Tangent Normal Brush Engine"
msgstr "Raaklijnnormaalpenseel-engine"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penseel-engine"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Normal Map"
msgstr "Normaal kaart"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:20
msgid ".. image:: images/icons/tangentnormal.svg"
msgstr ".. image:: images/icons/tangentnormal.svg"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:21
msgid ""
"The Tangent Normal Brush Engine is an engine that is specifically designed "
"for drawing normal maps, of the tangent variety. These are in turn used in "
"3d programs and game engines to do all sorts of lightning trickery. Common "
"uses of normal maps include faking detail where there is none, and to drive "
"transformations (Flow Maps)."
msgstr ""
"De Raaklijnnormaalpenseel-engine is een engine die speciaal is ontworpen "
"voor tekenen van normaal kaarten, van de raaklijn variëteit. Deze worden op "
"hun beurt gebruikt in 3d programma's en spel-engines om allerlei soorten van "
"belichtingstruken te doen. Gebruikelijk gebruik van normaalkaarten omvatten "
"het fingeren van details waar er geen zijn en om transformaties aan te "
"sturen (Flow-kaarten)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:23
msgid ""
"A Normal map is an image that holds information for vectors. In particular, "
"they hold information for Normal Vectors, which is the information for how "
"the light bends on a surface. Because Normal Vectors are made up of 3 "
"coordinates, just like colors, we can store and see this information as "
"colors."
msgstr ""
"Een normaalkaart is een afbeelding die informatie bevat voor vectors. In het "
"bijzonder bevatten ze informatie voor normaalvectors, wat de informatie is "
"voor hoe het licht buigt op een oppervlak. Omdat normaalvectors bestaan uit "
"3 coördinaten, net als kleuren, kunnen we deze informatie opslaan en zien "
"als kleuren."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:25
msgid ""
"Normals can be seen similar to the stylus on your tablet. Therefore, we can "
"use the tilt-sensors that are available to some tablets to generate the "
"color of the normals, which can then be used by a 3d program to do lighting "
"effects."
msgstr ""
"Normalen kunnen gezien worden als de stylus op uw tablet. Daarom kunnen we "
"de tilt-sensors gebruiken die beschikbaar zijn op sommige tablets om de "
"kleur van de normalen te genereren, die dan gebruikt worden door een 3d "
"programma om belichtingseffecten te doen."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:27
msgid "In short, you will be able to paint with surfaces instead of colors."
msgstr ""
"In het kort, u zult in staat zijn om met oppervlakken te schilderen in "
"plaats van kleuren."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:29
msgid "The following options are available to the tangent normal brush engine:"
msgstr ""
"De volgende opties zijn beschikbaar voor de raaklijnnormaalpenseel-engine:"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:31
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:32
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:33
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:34
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:35
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:36
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:37
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:38
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:39
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:40
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:41
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:42
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:43
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:46
msgid "Specific Parameters to the Tangent Normal Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:50
msgid "Tangent Tilt"
msgstr "Rakende helling"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:52
msgid ""
"These are the options that determine how the normals are calculated from "
"tablet input."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:54
msgid "Tangent Encoding"
msgstr "Raaklijncodering"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:55
msgid ""
"This allows you to set what each color channel means. Different programs set "
"different coordinates to different channels, a common version is that the "
"green channel might need to be inverted (-Y), or that the green channel is "
"actually storing the x-value (+X)."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:56
msgid "Tilt Options"
msgstr "Opties voor helling"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:57
msgid "Allows you to choose which sensor is used for the X and Y."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:58
msgid "Tilt"
msgstr "Scheef zetten"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:59
msgid "Uses Tilt for the X and Y."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:60
msgid "Direction"
msgstr "Richting"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:61
msgid ""
"Uses the drawing angle for the X and Y and Tilt-elevation for the Z, this "
"allows you to draw flowmaps easily."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:62
msgid "Rotation"
msgstr "Rotatie"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:63
msgid ""
"Uses rotation for the X and Y, and tilt-elevation for the Z. Only available "
"for specialized Pens."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid "Elevation Sensitivity"
msgstr "Gevoeligheid voor verhoging"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid ""
"Allows you to change the range of the normal that are outputted. At 0 it "
"will only paint the default normal, at 1 it will paint all the normals in a "
"full hemisphere."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:68
msgid "Usage"
msgstr "Gebruik"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:70
msgid ""
"The Tangent Normal Map Brush Engine is best used with the Tilt Cursor, which "
"can be set in :menuselection:`Settings --> Configure Krita --> General --> "
"Outline Shape --> Tilt Outline`."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:73
msgid "Normal Map authoring workflow"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:75
msgid "Create an image with a background color of (128, 128, 255) blue/purple."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:80
msgid "Setting up a background with the default color."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:82
msgid ""
"Set up group with a :guilabel:`Phong Bumpmap` filter mask. Use the :guilabel:"
"`Use Normal map` checkbox on the filter to make it use normals."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:87
msgid ""
"Creating a phong bump map filter layer, make sure to check 'Use Normal map'."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:92
msgid ""
"These settings give a nice daylight-esque lighting setup, with light 1 being "
"the sun, light 3 being the light from the sky, and light 2 being the light "
"from the ground."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:94
msgid ""
"Make a :guilabel:`Normalize` filter layer or mask to normalize the normal "
"map before feeding it into the Phong bumpmap filter for the best results."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:95
msgid "Then, paint on layers in the group to get direct feedback."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:100
msgid ""
"Paint on the layer beneath the filters with the tangent normal brush to have "
"them be converted in real time."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:102
msgid ""
"Finally, when done, hide the Phong bumpmap filter layer (but keep the "
"Normalize filter layer!), and export the normal map for use in 3d programs."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:105
msgid "Drawing Direction Maps"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:107
msgid ""
"Direction maps are made with the :guilabel:`Direction` option in the :"
"guilabel:`Tangent Tilt` options. These normal maps are used to distort "
"textures in a 3d program (to simulate for example, the flow of water) or to "
"create maps that indicate how hair and brushed metal is brushed. Krita can't "
"currently give feedback on how a given direction map will influence a "
"distortion or shader, but these maps are a little easier to read."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:109
msgid ""
"Just set the :guilabel:`Tangent Tilt` option to :guilabel:`Direction`, and "
"draw. The direction your brush draws in will be the direction that is "
"encoded in the colors."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:112
msgid "Only editing a single channel"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:114
msgid ""
"Sometimes you only want to edit a single channel. In that case set the "
"blending mode of the brush to :guilabel:`Copy <channel>`, with <channel> "
"replaced with red, green or blue. These are under the :guilabel:`Misc` "
"section of the blending modes."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:116
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to :guilabel:`Copy Red`."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:123
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with :guilabel:`Copy "
"Green` above it."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:126
msgid "Mixing Normal Maps"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:128
msgid ""
"For mixing two normal maps, Krita has the :guilabel:`Combine Normal Map` "
"blending mode under :guilabel:`Misc`."
msgstr ""
