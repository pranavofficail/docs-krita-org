# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:28+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita Scribus\n"

#: ../../general_concepts/file_formats/file_pdf.rst:1
msgid "The PDF file format in Krita."
msgstr "O formato PDF no Krita."

#: ../../general_concepts/file_formats/file_pdf.rst:10
msgid "*.pdf"
msgstr "*.pdf"

#: ../../general_concepts/file_formats/file_pdf.rst:10
msgid "PDF"
msgstr "PDF"

#: ../../general_concepts/file_formats/file_pdf.rst:15
msgid "\\*.pdf"
msgstr "\\*.pdf"

#: ../../general_concepts/file_formats/file_pdf.rst:17
msgid ""
"``.pdf`` is a format intended for making sure a document looks the same on "
"all computers. It became popular because it allows the creator to make sure "
"that the document looks the same and cannot be changed by viewers. These "
"days it is an open standard and there is quite a variety of programs that "
"can read and save PDFs."
msgstr ""
"O formato ``.pdf`` é um formato destinado a garantir que um documento "
"aparece igual em todos os computadores. Tornou-se popular porque permite ao "
"criador garantir que o documento tem o mesmo aspecto e não pode ser alterado "
"por quem o leia. Nos dias de hoje, é um formato aberto e já existe uma "
"grande quantidade de programas que conseguem ler e gravar PDF's."

#: ../../general_concepts/file_formats/file_pdf.rst:19
msgid ""
"Krita can open PDFs with multiple layers. There is currently no PDF export, "
"nor is that planned. If you want to create a PDF with images from Krita, use "
"`Scribus <https://www.scribus.net/>`_."
msgstr ""
"O Krita consegue abrir PDF's com várias camadas. De momento, não existe "
"nenhuma exportação para PDF, nem está planeada. Se quiser criar um PDF com "
"imagens do Krita, use o `Scribus <https://scribus.net/>`_."

#: ../../general_concepts/file_formats/file_pdf.rst:21
msgid ""
"While PDFs can be viewed via most browsers, they can also become very heavy "
"and are thus not recommended outside of official documents. Printhouses will "
"often accept PDF."
msgstr ""
"Embora os PDF's possam ser visualizados na maioria dos navegadores, também "
"se poderão tornar muito pesados e, como tal, não são recomendados fora do "
"contexto de documentos oficiais. As casas de impressão irão frequentemente "
"aceitar o formato PDF."
