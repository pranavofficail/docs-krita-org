# Translation of docs_krita_org_reference_manual___filters___other.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___other\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-21 15:08+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/filters/other.rst:1
msgid "Overview of the other filters."
msgstr "Огляд інших фільтрів."

#: ../../reference_manual/filters/other.rst:15
msgid "Other"
msgstr "Інші"

#: ../../reference_manual/filters/other.rst:17
msgid "Filters signified by them not fitting anywhere else."
msgstr "Фільтри, які не належать до якихось категорій."

#: ../../reference_manual/filters/other.rst:20
msgid "Wave"
msgstr "Хвиля"

#: ../../reference_manual/filters/other.rst:22
msgid "Adds a cute little wave-distortion effect to the input image."
msgstr "Додає чудовий ефекти викривлення малою хвилею на вхідне зображення."

#: ../../reference_manual/filters/other.rst:24
#: ../../reference_manual/filters/other.rst:27
msgid "Random Noise"
msgstr "Випадковий шум"

#: ../../reference_manual/filters/other.rst:29
msgid "Gives Random Noise to input image."
msgstr "Додає випадковий шум на вхідне зображення."

#: ../../reference_manual/filters/other.rst:32
msgid "Random Pick"
msgstr "Випадкове підбирання"

#: ../../reference_manual/filters/other.rst:34
msgid "Adds a little pixely-fringe to the input image."
msgstr "Додає невеличку піксельну бахрому на вхідне зображення."
