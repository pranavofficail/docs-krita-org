# Translation of docs_krita_org_reference_manual___tools___smart_patch.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-24 16:42+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:62
msgid ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: toolsmartpatch"
msgstr ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: eina de patró intel·ligent"

#: ../../reference_manual/tools/smart_patch.rst:1
msgid "Krita's smart patch tool reference."
msgstr "Referència de l'eina Patró intel·ligent del Krita."

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Smart Patch"
msgstr "Patró intel·ligent"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Automatic Healing"
msgstr "Automàtica de guariment"

#: ../../reference_manual/tools/smart_patch.rst:16
msgid "Smart Patch Tool"
msgstr "Eina de patró intel·ligent"

#: ../../reference_manual/tools/smart_patch.rst:18
msgid "|toolsmartpatch|"
msgstr "|toolsmartpatch|"

#: ../../reference_manual/tools/smart_patch.rst:20
msgid ""
"The smart patch tool allows you to seamlessly remove elements from the "
"image. It does this by letting you draw the area which has the element you "
"wish to remove, and then it will attempt to use patterns already existing in "
"the image to fill the blank."
msgstr ""
"L'eina de patró intel·ligent permet eliminar elements de la imatge sense "
"problemes. Per a això, permet que dibuixeu en l'àrea que conté l'element que "
"voleu eliminar i, després intentarà utilitzar els patrons ja existents a la "
"imatge per emplenar l'espai en blanc."

#: ../../reference_manual/tools/smart_patch.rst:22
msgid "You can see it as a smarter version of the clone brush."
msgstr "Es pot veure com una versió més intel·ligent del pinzell de clonat."

#: ../../reference_manual/tools/smart_patch.rst:25
msgid ".. image:: images/tools/Smart-patch.gif"
msgstr ".. image:: images/tools/Smart-patch.gif"

#: ../../reference_manual/tools/smart_patch.rst:26
msgid "The smart patch tool has the following tool options:"
msgstr ""
"L'eina de patró intel·ligent disposa de les següents Opcions de l'eina:"

#: ../../reference_manual/tools/smart_patch.rst:29
msgid "Accuracy"
msgstr "Precisió"

#: ../../reference_manual/tools/smart_patch.rst:31
msgid ""
"Accuracy indicates how many samples, and thus how often the algorithm is "
"run. A low accuracy will do few samples, but will also run the algorithm "
"fewer times, making it faster. Higher accuracy will do many samples, making "
"the algorithm run more often and give more precise results, but because it "
"has to do more work, it is slower."
msgstr ""
"La precisió indica quantes mostres i, per tant, amb quina freqüència "
"s'executa l'algorisme. Una baixa precisió farà poques mostres, però també "
"executarà l'algorisme menys vegades, fent-lo més ràpid. Una major precisió "
"farà moltes mostres, fent que l'algoritme s'executi amb més freqüència i "
"doni resultats més precisos, però a causa que haurà de fer més feina, serà "
"més lent."

#: ../../reference_manual/tools/smart_patch.rst:34
msgid "Patch size"
msgstr "Mida del patró"

#: ../../reference_manual/tools/smart_patch.rst:36
msgid ""
"Patch size determines how big the size of the pattern to choose is. This "
"will be best explained with some testing, but if the surrounding image has "
"mostly small elements, like branches, a small patch size will give better "
"results, while a big patch size will be better for images with big elements, "
"so they get reused as a whole."
msgstr ""
"La mida del patró determinarà com de gran serà el patró a triar. Això "
"s'explicarà millor amb algunes proves, però si la imatge circumdant conté "
"majoritàriament elements petits, com branques, una mida de patró petita "
"donarà millors resultats, mentre que una mida de patró gran serà millor per "
"a les imatges amb elements grans, de manera que es tornaran a utilitzar en "
"conjunt."
